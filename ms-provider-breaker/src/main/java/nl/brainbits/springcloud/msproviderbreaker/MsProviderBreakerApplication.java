package nl.brainbits.springcloud.msproviderbreaker;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@SpringBootApplication
@EnableEurekaClient
@EnableScheduling
@EnableCircuitBreaker
public class MsProviderBreakerApplication {

	@Autowired
	private BreakerService breakerController;

	public static void main(String[] args) {
		SpringApplication.run(MsProviderBreakerApplication.class, args);
	}

	@Scheduled(fixedDelay = 3000L)
	public void call() {
		// Fake a restcall :)
		breakerController.getSomething();
	}
}
